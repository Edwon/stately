 /*
    stately, a state machine system for C#
    Copyright (C) 2018 Evan Hemsley

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stately;

namespace Stately {
    namespace Example {
        public class Cube : MonoBehaviour {

            new Rigidbody rigidbody;
            Material material;
            Quaternion startRotation;

            State rootState = new State("root");

            public class JumpingState : State {
                public JumpingState(string name) : base(name) { }
                public State notRotating = new State("notRotating");
                public State rotating = new State("rotating");
            }

            State idleState = new State("idle");
            JumpingState jumpingState = new JumpingState("jumping");
            State fallingState = new State("falling");

            ParticleSystem dustParticles;

            void Awake() {
                rigidbody = GetComponent<Rigidbody>();
                dustParticles = transform.Find("DustParticles").GetComponent<ParticleSystem>();
                material = GetComponent<MeshRenderer>().material;

                startRotation = transform.rotation;

                DefineStateMachine();

                rootState.Start();
            }

            void DefineStateMachine() {
                rootState.StartAt(idleState);

                rootState.OnUpdate = delegate { Debug.Log(rootState.CurrentState.CurrentStatePath); };

                rootState.ChangeToSubState(idleState).If(() => Input.GetButtonDown("Reset")).ThenDo(delegate
                {
                    transform.position = new Vector3(0f, 0.6f, 0f);
                    transform.rotation = startRotation;
                    rigidbody.velocity = Vector3.zero;
                    rigidbody.angularVelocity = Vector3.zero;
                    SetColor(Color.red);
                });

                idleState.ChangeTo(jumpingState).If(() => Input.GetButton("Jump"))
                                                .AndIf(() => Input.GetButton("Fire1"))
                                                .OrAfter(2f)
                                                .ThenDo(delegate
                                                {
                                                    dustParticles.Emit(100);
                                                });

                idleState.OnTransitionTo(jumpingState).AlsoDo(delegate { dustParticles.Emit(1000); }); // adds to the transition callback
                idleState.OnTransitionTo(jumpingState).InsteadDo(delegate { }); // overrides the transition callback

                jumpingState.OnEnter = delegate
                {
                    rigidbody.AddForce(300f * Vector3.up);
                    SetColor(Color.yellow);
                };

                jumpingState.OnUpdate = delegate { };
                jumpingState.OnExit = delegate { SetColor(Color.green); };

                jumpingState.StartAt(jumpingState.notRotating);
                
                jumpingState.notRotating.ChangeTo(jumpingState.rotating).AfterNFrames(15); // If(() => Input.GetButtonDown("Jump"));

                jumpingState.rotating.OnEnter = delegate
                {
                    rigidbody.AddTorque(200f, 0f, 0f);
                    SetColor(Color.magenta);
                };

                jumpingState.ChangeTo(fallingState).If(() => rigidbody.velocity.y <= 0f).AndAfter(0.2f);

                fallingState.OnExit = delegate { SetColor(Color.red); };

                fallingState.ChangeTo(idleState).IfSignalCaught("groundCollision").ThenDo(delegate
                {
                    dustParticles.Emit(100);
                });

                fallingState.ReplaceTransitionCondition(idleState).With.IfSignalCaught("groundCollision");
            }
            
            void Update ()
            {
                rootState.Update(Time.deltaTime);
            }

            void FixedUpdate()
            {
                rootState.FixedUpdate();
            }

            void OnCollisionEnter(Collision col)
            {
                if (col.gameObject.name == "Floor")
                {
                    rootState.SendSignal("groundCollision");
                }
            }

            void SetColor(Color color)
            {
                material.SetColor("_Color", color);
            }
        }
    }
}
